<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'Home/Index';
$route['register'] = 'Home/Register';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
