<div class="container">
	<div class="register">
		<form class="register_form_inputs" action="/register" method="POST">
			<div class="register_form">
				<h1 style="color: white;">Register</h1>
					<p>Welcome to Ani-WoW. Join us today!</p>
					<hr>
					<label for="username" style="color: white;"><b>Username</b></label> <br>
						<input type="text" name="username" placeholder="Username" required>
					<label for="password" style="color: white;"><b>Password</b></label> <br>
						<input type="password" name="password" placeholder="Password" required>
					<label for="password" style="color: white;"><b>Confirm Password</b></label> <br>
						<input type="password" name="confirm_password" placeholder="Password" required>
					<label for="email" style="color: white;"><b>Email</b></label> <br>
						<input type="email" name="email" placeholder="Email" required>
			</div>
			<hr>
			<p>By creating an account, you agree to all <a href="/forum/index.php?app=forums&module=forums&controller=topic&id=2&tab=comments#comment-2" style="color: white;">rules and terms</a>.</p>
			<button type="submit" class="registerbtn">Register</button>			
		</form>
	</div>
	

	<div class="about">
		<div class="about_info">
			<h1 style="color: white;">Ani-WoW</h1>
			<p>Ani-WoW is here to satisfy all your WoW needs. Mists of Pandaria, we got that.</p>
			<p>You? Need that.</p> <br>
			<img src="/assets/img/arrows.png"> <br>
			<p>See those arrows? The one pointing to the right is how many people are online and enjoying the Ani-WoW experience.</p>
			<p>The arrow pointing to the left? That's how you join those people.</p> <br>
			<img style="max-width: 250px; max-height: 250px;" src="/assets/img/badge.png">
		</div>
	</div>


	<div class="status">
		<div class="status_info">
				<label><h1 style="color: white;">Berserk i90</h1></label>
				<p>Players Online: <?php print_r($online_players); ?></p>
				<p>Server uptime: <?php print_r(round($srv_uptime['uptime']/60/60)); ?> Hours</p>
				<p>SET realmlist "logon.ani-wow.com"</p>
			<br>
		</div>
		<iframe src="https://discordapp.com/widget?id=593109730378121259&theme=dark" allowtransparency="true" frameborder="0"></iframe>
	</div>	
</div>


