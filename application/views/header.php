<!DOCTYPE html>
<html>
<head>
	<title>Ani-WoW | 5.4 Mists of Pandaria</title>
	<meta charset="utf-8">
	<meta name="description" content="Ani-WoW is the BEST world of warcraft server for Mists of Pandaria. With daily development and careful curation of content, you'll never be bored. Come on in and sign up today!">
	<meta name="keywords" content="ani, wow, ani-wow, best, great, wow, world, of, warcraft, mists, pandaria, 5.4, mop, pve, pvp, rp, lag-free, private, server, discord, berserk, anime, molten, molten wow, warmane, serenity-wow, serenity">
	<meta name="author" content="Ani-WoW">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="/assets/style/style.css">
</head>
<body>

<img class="logo" src="/assets/img/badge.png" alt="Logo">

<div class="social"></div>
	

<div class="bar">
	<ul class="nav_item">
		<li><a href="/index.php">Home</a></li>
		<li><a href="/forum">Forums</a></li>
		<li><a href="https://www.discord.gg/bQUUarn" target="_blank">Discord</a></li>
		<li><a href="/downloads/Ani-WoW.rar">Download</a></li>
	</ul>
</div>
<br>

	<?php if($this->session->flashdata('error'))
					{ ?>
					<div id="error">There was an error, creating your account. Please try again.<br /> Error: <?php echo $this->session->flashdata('error')['message']; ?></div>
					<?php
					}
					?>
					<?php if($this->session->flashdata('success'))
					{ ?>
					<div id="success">Your account was successfully created!</div>
					<?php
					}
	?>

