<?php
class Db_check_model extends CI_Model {

    public function register_check($var, $check_against)
    {
        $this->load->database();
        $this->db->where($check_against, $var);
        $this->db->from('account');
        return $this->db->count_all_results();
    }
    public function players_online_check()
    {
        $characters = $this->load->database('characters', TRUE);
        $query = $characters->select('*')->from('Characters')->where('online', '1');
        return $query->get()->num_rows();
    }
    public function get_uptime()
    {
        $this->load->database();
        $this->db->select('*')->from('uptime')->order_by('starttime', 'desc')->limit("1");
        $query = $this->db->get();
        return $query->row_array();
    }

}






?>