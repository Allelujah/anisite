<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
	public function index()
	{
		
		$this->load->model('Db_check_model');
		$data['online_players'] = $this->Db_check_model->players_online_check();
		$data['srv_uptime'] = $this->Db_check_model->get_uptime();
  		$this->load->view("header");
  		$this->load->view("index", $data);
  		$this->load->view("footer");
	}
	public function register()
	{
		if(!$this->input->post('username') && !$this->input->post('password') && !$this->input->post('confirm_password') && !$this->input->post('email'))
		{
			$this->session->set_flashdata('error', array('message' => "Post data didn't register (?)")); 
			redirect("../", "refresh");
			return false;
		}
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$cpas = $this->input->post('confirm_password');
		$email = $this->input->post('email');

		if(strlen($password) < 6)
		{
			$this->session->set_flashdata('error', array('message' => "Password must be longer than 6 characters.")); 
			redirect("../", "refresh");
			return false;
		}
		if(strlen($username) < 4 OR strlen($username) > 32)
		{
			$this->session->set_flashdata('error', array('message' => "Username too short or long. (4/32)")); 
			redirect("../", "refresh");
			return false;
		}

		if(!$password === $cpas)
		{
			$this->session->set_flashdata('error', array('message' => "Passwords do not match.")); 
			redirect("../", "refresh");
			return false;
		}

		$this->load->model('Register_model');
		$this->load->model('Db_check_model');


		if($this->Db_check_model->register_check($username, "username") >= 1)
		{
			$this->session->set_flashdata('error', array('message' => "Username exists.")); 
			redirect("../", "refresh");
			return false;
		}
		if($this->Db_check_model->register_check($email, "email") >= 1)
		{
			$this->session->set_flashdata('error', array('message' => "E-mail exists.")); 
			redirect("../", "refresh");
			return false;
		}

		$hashed_pass = sha1(strtoupper($username) . ":" . strtoupper($password));
		$this->Register_model->register_account($username, $hashed_pass, $email);
		$this->session->set_flashdata('success', array('message' => "Account successfully created!")); 
		redirect("../", "refresh");
		
	}
}